import com.impinj.octane.ImpinjReader;
import com.impinj.octane.Tag;

import java.io.File;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.jar.Pack200;

public class MainApp {
    private static final String ROBOT_IP_ADDRESS = "1.2.3.4";
    private static final int ROBOT_PORT = 7171;

    private static final String READER_IP_ADDRESS = "169.254.253.11";

    private static final String ROBOT_PASSWORD = "admin\r";
    //public static String RFID_PATH = "C:\\Users\\Drew\\Desktop\\RoboStock\\tag_events.txt";
    public static String MAP_FILE = "new_demo.map";


    public static void main(String args[]) throws Exception {

        TelnetWrapper TC;
        TagReportListenerImplementation report;

        TC = new TelnetWrapper(ROBOT_IP_ADDRESS, ROBOT_PORT, ROBOT_PASSWORD);
        TC.clearStream();

        ImpinjReader reader = ImpinjWrapper.setup(READER_IP_ADDRESS);
        report = ImpinjWrapper.apply_settings(reader);

        reader.queryTags();
        Thread.sleep(1000);
        LinkedList<RoboStockUtils.StatusHolder> shList = TC.monitorStatus(10000, reader);
        //reader.queryTags();
        //Thread.sleep(5000);
        ImpinjWrapper.teardown(reader);
        LinkedList<Tag> tList = report.returnTags();

        if (tList == null)
            System.out.println("Tag list is empty");
        else {
            /*for (Tag t : tList) {
                System.out.println("tag: " + t.getEpc().toString() + ", " + t.getFirstSeenTime().getLocalDateTime() + ", " + t.getAntennaPortNumber());
                System.out.println("newline");
            }*/
            LinkedList<RoboStockUtils.TagHolder> thList = RoboStockUtils.TagHolder.readTagList(tList);
            System.out.println("Reading tag list");

            /*for (RoboStockUtils.TagHolder t : thList) {

                System.out.println(t.toString());
            }
            System.out.println("\n--------------\n");*/

            System.out.println("Correlating Robot position with tags");
            LinkedList<RoboStockUtils.TagPositionHolder> tphList = RoboStockUtils.TagPositionHolder.corelateTagPositions(shList, thList);

            System.out.println("Writing to map file");
            RoboStockUtils.TagPositionHolder.writeToMapFile(new File(MAP_FILE), tphList);
            //Iterator<RoboStockUtils.TagPositionHolder> iter = tphList.iterator();
        }
        TC.disconnect();


        return;

    }



}