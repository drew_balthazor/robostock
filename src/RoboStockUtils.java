import org.apache.commons.io.FileUtils;
import org.apache.commons.io.IOUtils;
import org.apache.commons.io.output.StringBuilderWriter;
import org.apache.commons.net.telnet.TelnetClient;
import sun.awt.image.ImageWatched;

import java.io.*;
import java.math.BigInteger;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.ParsePosition;
import java.util.*;
import java.text.SimpleDateFormat;
import java.util.regex.Pattern;
import com.impinj.octane.*;

/**
 * Created by Drew on 5/18/2016.
 */
public class RoboStockUtils {

    public static final SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");
    public static final String matchPattern = "(Location: \\d+ \\d+ \\d+)";

    public static void main(String[] args) throws Exception {
        String RFID_PATH = "C:\\Users\\Drew\\development\\RoboStock\\tests\\log1.txt";
        String ROBOT_LOG_PATH = "C:\\Users\\Drew\\development\\RoboStock\\tests\\robot_debug.txt";
        System.out.println("TESTING MODE ONLY");

        String test1 = "Status: DockingState: Docked ForcedState: Unforced ChargeState: Overcharge BatteryVoltage: 29.8 Location: 2386 1518 93 Temperature: -128";
        String test2 = "Patrolling route penhallway demo once";

        assert(true == test1.contains("Location: "));
        assert(false == test2.contains("Location: "));
        assert(true == RoboStockUtils.betweenDatesInclusive(new Date(1000L), new Date(1500L), new Date(2000L)));
        assert(false == RoboStockUtils.betweenDatesInclusive(new Date(1500L), new Date(1000L), new Date(2000L)));
        assert(false == RoboStockUtils.betweenDatesInclusive(new Date(1000L), new Date(2000L), new Date(1500L)));
        assert(false == RoboStockUtils.betweenDatesInclusive(new Date(2000L), new Date(1500L), new Date(1000L)));

        File robot_log = new File(ROBOT_LOG_PATH);
        File rfid_log = new File(RFID_PATH);

        if (!robot_log.exists()) {
            createSampleLog(robot_log);
        }
        if (!rfid_log.exists())
            throw new Exception("RFID Log not found, exiting");

        System.out.println("Printing Tag List from log");
        /*LinkedList<TagHolder> taglist =  TagHolder.parseRFIDLog(rfid_log);
        Iterator<TagHolder> iter = taglist.iterator();

        while (iter.hasNext()) {
            System.out.println(iter.next().toString());
        }
        long startDate = (SDF.parse("2016-05-11T23:20:25.000", new ParsePosition(0))).getTime();

        LinkedList<StatusHolder> shList = new LinkedList<>();
        int x;

        for (x = 0; x < 20; x++) {
            shList.add(new StatusHolder((10 * x), (-10 * x), 0, startDate + (500 * x)));
        }

        Iterator<StatusHolder> shIter = shList.iterator();

        System.out.println("\nPrinting status list");

        while (shIter.hasNext()) {
            System.out.println(shIter.next().toString());
        }

        //System.out.println("\nPrinting corleated tag list");
        LinkedList<TagPositionHolder> tphList = TagPositionHolder.corelateTagPositions(shList, taglist);

        Iterator<TagPositionHolder> tphIter = tphList.iterator();

        File map = new File("C:\\Users\\Drew\\Desktop\\RoboStock\\lab1.map");
        TagPositionHolder.writeToMapFile(map, tphList);*/

        //while (tphIter.hasNext())
            //System.out.println(tphIter.next().toString());

        return;
    }

    public static void createSampleLog (File log) throws IOException, InterruptedException {
        //Used for creating test log, uncomment for use
        //File log = new File(location);

        Random rand = new Random();
        OutputStream OuSt = new FileOutputStream(log);
        int x;
        for (x = 0; x < 72;) {
            x++;
            RoboStockUtils.sendCommand("Line " + x + '\n', OuSt);
        }
        for (x = 0; x <16; x++) {
            String test = "Status: DockingState: Docked ForcedState: Unforced ChargeState: Overcharge BatteryVoltage: 29.8 Location: "
                    + rand.nextInt(5000) + " " + rand.nextInt(5000) + " " + rand.nextInt(360) + " Temperature: -128\n";
            RoboStockUtils.sendCommand(test, OuSt);
            //Thread.sleep(200);

        }
        OuSt.flush();
        OuSt.close();

        return;
    }


    public static boolean betweenDatesInclusive (Date after, Date middle, Date before) {
        //determines whether one date is between two other dates, inclusive

        if (after.after(before))
            return false;
        assert(after.after(before)); //Check to make sure the first date is before the second date


        long longBefore = before.getTime();
        long longAfter = after.getTime();
        long longMiddle = middle.getTime();

        if (longMiddle > longBefore)
            return false;
        else if (longMiddle < longAfter)
            return false;
        else
            return true;

    }

    //Reads a line from the inputstream, returns as string
    //WILL BLOCK IF NOTHING TO BE READ
    public static String readCommand(InputStream IS) throws java.io.IOException {
        byte[] b = new byte[2048];
        IOUtils.read(IS, b);
        return new String(b, "UTF-8");
    }

    //Sends a string to an OutputStream
    //Will need to append carriage return ('\r') in order for robot to execute command
    public static void sendCommand(String arg, OutputStream OS) throws java.io.IOException{
        //StringBuilder toSend = new StringBuilder(arg);

        //toSend.append('\r');
        byte[] b = arg.getBytes();
        OS.write(b);
        OS.flush();
        return;
    }

    static class TagHolder {
        protected Date time;
        //long tagId;
        String tagIdString;

        public TagHolder (Date date, //long tagID,
                          String tagString) {
            time = date;
            //tagId = tagID;
            tagIdString = tagString;
        }

        public TagHolder (long date, //long tagID,
                          String tagString) {
            time = new Date(date);
            //tagId = tagID;
            tagIdString = tagString;
        }

        public static LinkedList<TagHolder> readTagList(LinkedList<Tag> tagList) throws OctaneSdkException {
            LinkedList<TagHolder> newTagList = new LinkedList<>();
            TagHolder temp;

            if (tagList == null)
                return null;

            for (Tag t : tagList) {
                //System.out.println("tag: " + t.getEpc().toString() + ", " + t.getFirstSeenTime().getLocalDateTime() + ", " + t.getAntennaPortNumber());
                newTagList.addFirst(new TagHolder(
                        t.getFirstSeenTime().getLocalDateTime(),
                        //t.getEpc().toDoubleWord(),
                        t.getEpc().toString()
                        ));

            }
            return newTagList;
        }

        /*public static LinkedList<TagHolder> parseRFIDLog(File fileName) throws IOException {
            //Path path = Paths.get(fileName);
            LinkedList<TagHolder> taglist = new LinkedList<>();

            // Initiate Scanner Object
            Scanner scan = new Scanner(fileName);
            // initialize string delimiter
            scan.useDelimiter("(\\s*event.tag.arrive tag_id=0x|, first=)\\s*");
            //SimpleDateFormat SDF = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss");

            String temp;
            BigInteger tempBigInt;
            String tempDate;
            Date parsedDate;
            while (scan.hasNext())
            {
                temp = (scan.next()).trim();
                tempBigInt = new BigInteger(temp, 16);
                tempDate = (scan.next()).trim();
                parsedDate = SDF.parse(tempDate, new ParsePosition(0));
                //System.out.println("tagid = " + tempBigInt);

                //System.out.println("date =  " + parsedDate);
                taglist.add(new TagHolder(parsedDate, tempBigInt, temp));
                // new element gets these properties and try to put into list
            }
            // closing the scanner stream
            scan.close();
            return taglist;
        }*/

        @Override
        public String toString() {
            return "TagHolder{" +
                    "time=" + time +
                    //", tagId=" + tagId +
                    ", tagIdString='" + tagIdString + '\'' +
                    '}';
        }
    }



    static class StatusHolder {
        //Holds position and heading for robot, use for list of positions with timestamp
        protected int pos_x;
        protected int pos_y;
        protected int pos_th;
        protected Date time; //TODO: make sure this agrees with Peter's time thing

        public StatusHolder (int x_coord, int y_coord, int heading) {
            pos_x = x_coord;
            pos_y = y_coord;
            pos_th = heading;
            time = new Date();
        }
        public StatusHolder (int x_coord, int y_coord, int heading, long date) {
            pos_x = x_coord;
            pos_y = y_coord;
            pos_th = heading;
            time = new Date(date);
        }

        public static StatusHolder parseStatus(String status) {
            //status format is “Status “<status> “Battery Voltage: “<VDC> “Location “ <x> <y> <th>

            //System.out.println(status);
            status = status.substring(status.indexOf("Location:"));

            String[] strArr = status.split("\\s");
            //System.out.println(strArr[0]);                    //"Location: "
            //System.out.println(Integer.parseInt(strArr[1]));  // "<x value string>"
            //System.out.println(Integer.parseInt(strArr[2]));  // "<y value string>"
            //System.out.println(Integer.parseInt(strArr[3]));  // "<heading value string>"
            StatusHolder ret = new StatusHolder(Integer.parseInt(strArr[1]), Integer.parseInt(strArr[2]), Integer.parseInt(strArr[3]));

            return ret;

        }


        @Override
        public String toString() {
            return "StatusHolder{" +
                    "pos_x=" + pos_x +
                    ", pos_y=" + pos_y +
                    ", pos_th=" + pos_th +
                    ", time=" + time.toString() +
                    '}';
        }
    }

    static class TagPositionHolder {
        protected int pos_x;
        protected int pos_y;
        protected TagHolder tag;
        protected Date time;

        public TagPositionHolder (int x_coord, int y_coord, TagHolder data, Date date) {
            pos_x = x_coord;
            pos_y = y_coord;
            tag = data;
            time = date;
        }

        public static void writeToMapFile(File file, LinkedList<TagPositionHolder> tphList) throws IOException {
            List<String> stringList = FileUtils.readLines(file, "UTF-8");
            PrintWriter mapWrite = new PrintWriter(file);
            Iterator<String> iter = stringList.iterator();
            Iterator<TagPositionHolder> tagIter = tphList.iterator();

            String temp;
            TagPositionHolder tempTPH;
            mapWrite.write("");
            while (iter.hasNext()) {
                temp = iter.next();
                if (temp.equalsIgnoreCase("LINES")) {
                    //System.out.println("Line found");
                    while (tagIter.hasNext()) {
                        tempTPH = tagIter.next();
                        //System.out.printf("Cairn: Label %d %d 0 \"TagID: %s\" ICON \"X\"\n", tempTPH.pos_x, tempTPH.pos_y, tempTPH.tag.tagIdString);
                        mapWrite.format("Cairn: Label %d %d 0 \"TagID: %s\" ICON \"X\"\n", tempTPH.pos_x, tempTPH.pos_y, tempTPH.tag.tagIdString);
                    }
                }
                //System.out.println(temp);
                mapWrite.println(temp);
            }

            //mapWrite.println("testing");
            mapWrite.flush();
            mapWrite.close();

            return;


        }

        public static LinkedList<TagPositionHolder> corelateTagPositions(LinkedList<StatusHolder> SHList, LinkedList<TagHolder> TagList) throws Exception {
            //Corlates Tag acquisition times with robots position and time
            //Assumes Status List has more than two entries in the list

            LinkedList<TagPositionHolder> corelatedList = new LinkedList<>();

            ListIterator<TagHolder> TagIterator = TagList.listIterator();
            ListIterator<StatusHolder> SHIterator = SHList.listIterator();

            TagHolder currTag;
            StatusHolder preStatus;
            StatusHolder postStatus;

            //int x;
            //for (x = 0; x < 1; x++) {
            while (TagIterator.hasNext()) {
                currTag = TagIterator.next();
                //System.out.println(currTag.toString());
                preStatus = SHIterator.next();
                postStatus = SHIterator.next();
                while (!betweenDatesInclusive(preStatus.time, currTag.time, postStatus.time) && SHIterator.hasNext()) {
                    preStatus = postStatus;
                    postStatus = SHIterator.next();
                }
                if (!betweenDatesInclusive(preStatus.time, currTag.time, postStatus.time) && !SHIterator.hasNext()) {
                    System.out.println("PreStatus time = " + preStatus.time);
                    System.out.println("PostStatus time = " + postStatus.time);
                    System.out.println("currTag time = " + currTag.time);
                    System.out.println("Tag date out of bounds, discarding");
                    //throw new Exception("Tag date out of bounds");
                }

                else {
                    //System.out.println("pre: " + preStatus.toString());
                    //System.out.println("post: " + postStatus.toString());
                    //System.out.println("");
                    float ratio = (currTag.time.getTime() - preStatus.time.getTime()) / ((float) (postStatus.time.getTime() - preStatus.time.getTime()));
                    //System.out.println("ratio = " + ratio);
                    int newX = Math.round((postStatus.pos_x - preStatus.pos_x) * ratio) + preStatus.pos_x;
                    int newY = Math.round((postStatus.pos_y - preStatus.pos_y) * ratio) + preStatus.pos_y;
                    corelatedList.add(new TagPositionHolder(newX, newY, currTag, currTag.time));
                }

                while (SHIterator.hasPrevious())
                    SHIterator.previous();

            }
            return corelatedList;
        }

        @Override
        public String toString() {
            return "TagPositionHolder{" +
                    "pos_x=" + pos_x +
                    ", pos_y=" + pos_y +
                    ", time=" + time +
                    '}' +
                    ",\n tag_data=" + tag.toString();
        }
    }
}
