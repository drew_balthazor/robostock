import com.impinj.octane.ImpinjReader;
import com.impinj.octane.OctaneSdkException;
import org.apache.commons.net.telnet.TelnetClient;

import java.io.*;
import java.util.LinkedList;
import java.util.Random;
import java.util.Scanner;
import java.util.concurrent.RunnableFuture;

/**
 * Created by Drew on 5/20/2016.
 */
public class TelnetWrapper {
    TelnetClient tc;
    InputStream IS;
    OutputStream OS;

    public TelnetWrapper(String ip, int port, String password) throws IOException, InterruptedException {
        tc = new TelnetClient();
        tc.connect(ip, port);

        IS = tc.getInputStream();
        OS = tc.getOutputStream();

        Thread.sleep(200);
        RoboStockUtils.sendCommand(password, OS);

    }

    public void disconnect() throws IOException {
        IS.close();
        OS.close();
        tc.disconnect();
    }

    public void clearStream() {
        Scanner streamClear = new Scanner(this.IS, "UTF-8"); //Clears the stream of the help message
        int x;
        String temp;
        for (x = 0; x < 71; x++) {                //TODO: find better way to do this
            temp = streamClear.nextLine();
            //System.out.println(temp);
            //System.out.println("Counter is: " + x);
        }
        //streamClear.close();
    }

    public LinkedList<RoboStockUtils.StatusHolder> monitorStatus(int waitTime, ImpinjReader reader) throws IOException, InterruptedException, OctaneSdkException {

        File debug = new File("debug_status.txt");
        if (!debug.exists())
            debug.createNewFile();

        PrintWriter pw = new PrintWriter(debug);
        pw.write("");

        LinkedList<RoboStockUtils.StatusHolder> shList = new LinkedList<>();
        RoboThread runner = new RoboThread();

        Scanner sysScan = new Scanner(System.in);
        Scanner stream = new Scanner(IS);

        //System.out.println("Type 'start' to begin monitoring");
        //while(!sysScan.next().equalsIgnoreCase("start"));

        runner.inScan = sysScan;
        runner.parent = Thread.currentThread();
        Thread t = new Thread(runner);
        t.start();

        String temp;
        RoboStockUtils.StatusHolder tempStatus;

        RoboStockUtils.sendCommand("onelineStatus\r", OS);
        temp = stream.nextLine();
        if (temp.contains("Location: ")) {
            tempStatus = RoboStockUtils.StatusHolder.parseStatus(temp);
            pw.println(tempStatus.toString());
            shList.add(tempStatus);
        }

        /*Thread.sleep(1000);
        try {
            reader.queryTags();
        } catch (OctaneSdkException e) {
            e.printStackTrace();
        }*/

        System.out.println("Monitor running, type 'stop' to end the loop.");
        //System.out.println("Robot will automatically dock once finished patrolling");
        RoboStockUtils.sendCommand("say Monitoring status\r", OS);
        Thread.sleep(5000);
        reader.start();
        RoboStockUtils.sendCommand("patrolOnce Route1\r", OS);
        System.out.println(stream.nextLine());
        while (runner.keepRunning) {
            RoboStockUtils.sendCommand("onelineStatus\r", OS);
            temp = stream.nextLine();
            if (temp.contains("Location: ")) {
                tempStatus = RoboStockUtils.StatusHolder.parseStatus(temp);
                pw.println(tempStatus.toString());
                shList.add(tempStatus);
            }
            try {
                Thread.sleep(waitTime);
            } catch (InterruptedException e) {
                //e.printStackTrace();
            }
        }

        try {
            reader.queryTags();
        } catch (OctaneSdkException e) {
            e.printStackTrace();
        }
        reader.stop();
        reader.queryTags();
        Thread.sleep(10000);

        t.join();
        pw.flush();
        pw.close();

        RoboStockUtils.sendCommand("onelineStatus\r", OS);
        temp = stream.nextLine();
        if (temp.contains("Location: ")) {
            tempStatus = RoboStockUtils.StatusHolder.parseStatus(temp);
            pw.println(tempStatus.toString());
            shList.add(tempStatus);
        }


        System.out.println("Loop ended");
        //RoboStockUtils.sendCommand("say Heading Home mother fucker/r", OS);
        //Thread.sleep(5000);
        RoboStockUtils.sendCommand("say Docking\r", OS);
        RoboStockUtils.sendCommand("dock\r", OS);

        return shList;
    }

    public class RoboThread implements Runnable {
        volatile boolean keepRunning = true;
        volatile Scanner inScan;
        volatile Thread parent;


        @Override
        public void run() {
            while (!inScan.next().equalsIgnoreCase("stop"));
            parent.interrupt();
            keepRunning = false;
            return;
        }
    }

}
