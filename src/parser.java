import java.io.IOException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;
import java.lang.String;
import java.math.BigInteger;


/**
 * Created by Peter on 5/9/2016.
 *
 * Moved and refactored to RoboStockUtils under TagHolder
 * Kept here for reference
 */
public class parser
{
    public static void main(String args[]) throws IOException
    {
        BigInteger tagId;
        String tagIdString;

        // Get path
        String fileName = "C:\\log1.txt";
        Path path = Paths.get(fileName);

        // Initiate Scanner Object
        Scanner scan = new Scanner(path);
        // initialize string delimiter
        scan.useDelimiter("(\\s*event.tag.arrive tag_id=0x|, first=)\\s*");
        while (scan.hasNext())
        {
            tagIdString = scan.next();
            tagId = new BigInteger(tagIdString, 16);
            System.out.println("tagid = " + tagId);

            System.out.println("date =  " + scan.next());
            // new element gets these properties and try to put into list
        }
        // closing the scanner stream
        scan.close();
    }
}

// make a new class with an element containing (tagId, time)
// make a list of elements of that class
// put into list only if tagId is not already in list
