/**
 * Created by Drew on 11/5/2016.
 */

import com.impinj.octane.*;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class ImpinjWrapper {
    public static ImpinjReader setup(String hostname) throws OctaneSdkException {
        ImpinjReader reader = new ImpinjReader();

        System.out.println("Connecting");
        reader.connect(hostname);
        return reader;

    }

    public static void teardown(ImpinjReader reader) throws OctaneSdkException {
        reader.stop();
        reader.disconnect();
    }

    public static TagReportListenerImplementation apply_settings(ImpinjReader reader) throws OctaneSdkException {
        Settings settings = reader.queryDefaultSettings();

        ReportConfig report = settings.getReport();
        report.setIncludeAntennaPortNumber(true);

        // don't get reports until we ask for them
        report.setMode(ReportMode.WaitForQuery);

        settings.setReaderMode(ReaderMode.AutoSetDenseReader);

        // set some special settings for antenna 1
        AntennaConfigGroup antennas = settings.getAntennas();
        antennas.disableAll();
        //antennas.enableById(new short[]{1,2, 3, 4});
        antennas.enableById(new short[]{1, 3});

        antennas.setIsMaxRxSensitivity(true);
        antennas.setIsMaxTxPower(true);
        antennas.setTxPowerinDbm(10.0);
        antennas.setRxSensitivityinDbm(-70);

        settings.getReport().setIncludeAntennaPortNumber(true);
        settings.getReport().setIncludeSeenCount(true);
        settings.getReport().setIncludeFirstSeenTime(true);

        TagReportListenerImplementation reporter = new TagReportListenerImplementation();

        reader.setTagReportListener(reporter);

        // since we are not getting reports until we ask, we may get buffer
        // filling
        reader.setBufferOverflowListener(new BufferOverflowListenerImplementation());
        reader.setBufferWarningListener(new BufferWarningListenerImplementation());

        System.out.println("Applying Settings");
        reader.applySettings(settings);
        return reporter;
    }

    public static void main(String[] args) {

        TagReportListenerImplementation report;
        List<Tag> tagList;

        try {
            String hostname = "169.254.253.11";

            ImpinjReader reader = setup(hostname);
            report = apply_settings(reader);

            System.out.println("Starting");
            reader.start();

            //System.out.println("Press Enter to continue and read all tags.");
            System.out.println("Scanning tags for 10 seconds");
            Thread.sleep(10000);
            reader.queryTags();
            Thread.sleep(5000);

            LinkedList<Tag> test = report.returnTags();
            LinkedList<RoboStockUtils.TagHolder> tList = RoboStockUtils.TagHolder.readTagList(test);
            if (tList == null) {
                System.out.println("Tag list empty");
            }
            else {
                for (RoboStockUtils.TagHolder th : tList) {
                    System.out.println(th.toString());

                }
            }
            teardown(reader);

            //Iterator<RoboStockUtils.TagHolder> tIter = tList.iterator();

            //while (tIter.hasNext()) {
            //    tIter.next().toString();
            //}


        } catch (OctaneSdkException ex) {
            System.out.println(ex.getMessage());
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
            ex.printStackTrace(System.out);
        }
    }
}

